script.on_event(defines.events.on_player_joined_game, function(event)
    local player = game.players[event.player_index]

    if player.name == 'clock_player' and player.controller_type ~= defines.controllers.cutscene then
        player.teleport({0, 0})

        local waypoints = 
        {
            {
              position = {-130, -30},
              transition_time = 100,
              time_to_wait = 60*60*60*24*365, -- about 4 years at 60ups 
              zoom = 0.3
            }
        }

        local cutscene = 
        {
          type=defines.controllers.cutscene,
          waypoints = waypoints,
          final_transition_time = 100
        }
        
        player.set_controller(cutscene)
    end
end)

script.on_event(defines.events.on_tick, function(event)
    if global.train_events and global.train_events[game.tick] ~= nil then
        if global.train_events[game.tick] == "delay_set_time" then
            delay_set_time()
        end
        if global.train_events[game.tick] == "release_trains" then
            release_trains()
        end

        global.train_events[game.tick] = nil
    end
end)

set_time = function(hr_1, hr_2, min_1, min_2, chest_data)
    if global.train_events == nil then global.train_events = {} end

    global.hr_1 = hr_1
    global.hr_2 = hr_2
    global.min_1 = min_1
    global.min_2 = min_2
    global.chest_data = chest_data

    local old_hr_1 = global.chest_data.hr_1.get_item_count("wooden-chest")
    local old_hr_2 = global.chest_data.hr_2.get_item_count("wooden-chest")
    local old_min_1 = global.chest_data.min_1.get_item_count("wooden-chest")
    local old_min_2 = global.chest_data.min_2.get_item_count("wooden-chest")

    if hr_1 ~= old_hr_1 or hr_2 ~= old_hr_2 or min_1 ~= old_min_1 or min_2 ~= old_min_2 then
        recall_trains()

        -- delay the time setting so the trains waiting at the reset station don't start moving immediately
        global.train_events[game.tick + 1] = "delay_set_time"
    end
end

delay_set_time = function()
    global.chest_data.hr_1.clear_items_inside()
    global.chest_data.hr_2.clear_items_inside()
    if global.hr_1 > 0 then
        global.chest_data.hr_1.insert({ name="wooden-chest", count=global.hr_1 })
    end
    if global.hr_2 > 0 then
        global.chest_data.hr_2.insert({ name="wooden-chest", count=global.hr_2 })
    end

    global.chest_data.min_1.clear_items_inside()
    global.chest_data.min_2.clear_items_inside()
    if global.min_1 > 0 then
        global.chest_data.min_1.insert({ name="wooden-chest", count=global.min_1 })
    end
    if global.min_2 > 0 then
        global.chest_data.min_2.insert({ name="wooden-chest", count=global.min_2 })
    end

    global.train_events[game.tick + 60*25] = "release_trains"
end

recall_trains = function()
    global.chest_data.reset.insert("wooden-chest")
end

release_trains = function()
    global.chest_data.reset.clear_items_inside()
end

remote.add_interface("clock", {set=set_time})
