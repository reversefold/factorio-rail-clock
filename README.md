# Factorio rail clock

This is a clock for factorio which displays the current time using trains on tracks as seven segment displays.

## To run
- Install [pipenv](https://github.com/pypa/pipenv).
- Run `pipenv shell; pyenv install`, this should install the necessary dependencies.
- Copy `clock_config.py.example` to `clock_config.py`, and edit accordingly.
- Run `./clock.py`
- Wait a minute for it to start the server, then start a client and connect.

## Why python? Why is it multiplayer?
Most of the logic is done in combinators, but there is no builtin capability to get the real-world time
in factorio. There are several chests at the bottom of the map. Each of these controls the number displayed
on one of the segments.
Every second, we paste a console command into the server, which sets the number of items in these chests.
Since we need to paste in commands like this, the game has to be mp. And since some code needs to be involved to do that,
there is a python script to coordinate the process.